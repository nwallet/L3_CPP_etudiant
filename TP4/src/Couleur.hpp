#ifndef COULEUR_HPP
#define COULEUR_HPP

struct Couleur {
   double _r,_g,_b;

   Couleur() {}
   Couleur(int r,int g, int b): _r(r),_g(g),_b(b) {}
};

#endif // COULEUR_HPP
