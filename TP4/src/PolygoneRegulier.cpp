#include "PolygoneRegulier.hpp"
#include "math.h"
#include <iostream>

PolygoneRegulier::PolygoneRegulier(Couleur const & couleur, Point const & centre, int rayon, int nbCotes)
    : FigureGeometrique(couleur), _nbPoints(nbCotes)
{
    _points = new Point [nbCotes];
    for(int i=0; i < nbCotes; i++){
       _points[i] = Point(cos(2*M_PI/nbCotes*i)*rayon+centre._x,sin(2*M_PI/nbCotes*i)*rayon+centre._y);
    }
}

PolygoneRegulier::~PolygoneRegulier(){
  delete [] _points;
}

void PolygoneRegulier::afficher() const{

    std::cout<<"PolygoneRegulier "
            <<getCouleur()._r<<"_"<<getCouleur()._g<<"_"<<getCouleur()._b<<" ";

    for(int i =0 ; i < _nbPoints ; i++){
        std::cout<<_points[i]._x<<"_"<<_points[i]._y<<" ";
     }

    std::cout<<std::endl;
}

Point & PolygoneRegulier::getPoint(int indice) const{
    return _points[indice];
}

int PolygoneRegulier::getNbPoints() const{
    return _nbPoints;
}
