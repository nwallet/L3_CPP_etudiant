#ifndef LIGNE_HPP
#define LIGNE_HPP

#include "FigureGeometrique.hpp"
#include "Point.hpp"

class Ligne : public FigureGeometrique
{
   private:
    Point _p0,_p1;

    public:
        Ligne(Couleur const & couleur, Point const & p0, Point const & p1);

        void afficher() const override;

        Point const & getP0() const;
        Point const & getP1() const;
};

#endif // LIGNE_HPP
