#ifndef POINT_HPP
#define POINT_HPP

struct Point
{
    int _x,_y;

    Point() {}
    Point(int x, int y): _x(x),_y(y) {}
};

#endif // POINT_HPP
