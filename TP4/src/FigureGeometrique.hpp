#ifndef FIGUREGEOMETRIQUE_HPP
#define FIGUREGEOMETRIQUE_HPP
#include "Couleur.hpp"


class FigureGeometrique
{
protected:
    Couleur _couleur;
public:
    FigureGeometrique(Couleur const & couleur);
    Couleur const & getCouleur() const;
    virtual void afficher() const;
};

#endif // FIGUREGEOMETRIQUE_HPP
