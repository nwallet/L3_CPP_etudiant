#include "FigureGeometrique.hpp"
#include "Ligne.hpp"
#include "PolygoneRegulier.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupLigne) { };

TEST(GroupLigne, Ligne_test1)  {
    Ligne L = Ligne(Couleur(1,0,0),Point(0,0),Point(100,200));
    CHECK_EQUAL(L.getP0()._x,0);
    CHECK_EQUAL(L.getP0()._y,0);

    CHECK_EQUAL(L.getP1()._x,100);
    CHECK_EQUAL(L.getP1()._y,200);
}

TEST(GroupLigne, Ligne_test2)  {
    Ligne L = Ligne(Couleur(1,0,0),Point(0,0),Point(100,200));
    CHECK_EQUAL(L.getCouleur()._r,1);
    CHECK_EQUAL(L.getCouleur()._g,0);
    CHECK_EQUAL(L.getCouleur()._b,0);
}


TEST_GROUP(GroupPolygone) { };

TEST(GroupPolygone, Polygone_test1)  {
    PolygoneRegulier L = PolygoneRegulier(Couleur(0,1,0),Point(100,200),50,5);
    CHECK_EQUAL(L.getPoint(0)._x,150);
    CHECK_EQUAL(L.getPoint(0)._y,200);

    CHECK_EQUAL(L.getPoint(1)._x,115);
    CHECK_EQUAL(L.getPoint(1)._y,247);
}

TEST(GroupPolygone, Polygone_test2)  {
    PolygoneRegulier L = PolygoneRegulier(Couleur(0,1,0),Point(100,200),50,5);
    CHECK_EQUAL(L.getNbPoints(),5);
}

TEST(GroupPolygone, Polygone_test3)  {
    PolygoneRegulier L = PolygoneRegulier(Couleur(0,1,0),Point(100,200),50,5);
    CHECK_EQUAL(L.getCouleur()._r,0);
    CHECK_EQUAL(L.getCouleur()._g,1);
    CHECK_EQUAL(L.getCouleur()._b,0);
}

