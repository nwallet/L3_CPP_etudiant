#include "Ligne.hpp"
#include <iostream>

Ligne::Ligne(Couleur const & couleur, Point const & p0, Point const & p1) : FigureGeometrique(couleur),_p0(p0),_p1(p1){

}

void Ligne::afficher()const{
    std::cout<<"Ligne "
           <<getCouleur()._r<<"_"<<getCouleur()._g<<"_"<<getCouleur()._b<<" "
           <<_p0._x<<"_"<<_p0._y<<" "
           <<_p1._x<<"_"<<_p1._y<<std::endl;
}

Point const & Ligne::getP0() const{
    return _p0;
}
Point const & Ligne::getP1() const{
    return _p1;
}
