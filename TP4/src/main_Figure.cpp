#include "FigureGeometrique.hpp"
#include "Ligne.hpp"
#include "PolygoneRegulier.hpp"

#include <iostream>
#include <vector>

int main() {
    Ligne L = Ligne(Couleur(1,0,0),Point(0,0),Point(100,200));
    L.afficher();

    PolygoneRegulier P = PolygoneRegulier(Couleur(0,1,0),Point(100,200),50,5);
    P.afficher();


    std::vector<FigureGeometrique> F {
        Ligne(Couleur(1,0,0),Point(0,0),Point(100,200)),
        PolygoneRegulier(Couleur(0,1,0),Point(100,200),50,5)
    };

    for (FigureGeometrique  each : F)
        each.afficher();

    return 0;
}
