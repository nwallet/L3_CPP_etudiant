#ifndef POLYGONEREGULIER_HPP
#define POLYGONEREGULIER_HPP

#include "FigureGeometrique.hpp"
#include "Point.hpp"

class PolygoneRegulier : public FigureGeometrique
{
private:
    int _nbPoints;
    Point * _points;

public:
    PolygoneRegulier(Couleur const & couleur, Point const & centre, int rayon, int nbCotes);
    ~PolygoneRegulier();

    void afficher() const override;

    int getNbPoints() const;
    Point & getPoint(int indice) const;
};

#endif // POLYGONEREGULIER_HPP
