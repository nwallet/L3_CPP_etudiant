#include "vecteur3.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupVecteur3) { };

TEST(GroupVecteur3, test_norme) {  // premier test
    vecteur3 v {2.0f,6.0f,3.0f};
    float result = v.norme();
    CHECK_EQUAL(7, result);
}

TEST(GroupVecteur3, test_produit_scalaire) {  // premier test
    vecteur3 v {2.0f,3.0f,6.0f};
    vecteur3 w {6.0f,3.0f,2.0f};
    float result = produitScalaire(v,w);
    CHECK_EQUAL(33, result);
}

TEST(GroupVecteur3, test_addition) {  // premier test
    vecteur3 v {2.0f,3.0f,6.0f};
    vecteur3 w {6.0f,3.0f,2.0f};
    vecteur3 result = addition(v,w);
    CHECK_EQUAL(8, result.x);
    CHECK_EQUAL(6, result.y);
    CHECK_EQUAL(8, result.z);
}
