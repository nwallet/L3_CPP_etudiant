#include "vecteur3.hpp"
#include<iostream>
#include <math.h>

void vecteur3::afficher()const{
   std::cout<<"( "<<x<<" , "<<y<<" , "<<z<<" )"<<std::endl;
}

void afficher(const vecteur3 &v){
   std::cout<<"( "<<v.x<<" , "<<v.y<<" , "<<v.z<<" )"<<std::endl;
}

float vecteur3::norme()const{
    return sqrt(x*x+y*y+z*z);
}

float produitScalaire(const vecteur3 &v, const vecteur3 & w){
    return v.x*w.x+v.y*w.y+v.z*w.z;
}

vecteur3 addition(const vecteur3 &v, const vecteur3 & w){
    return vecteur3 {v.x+w.x,v.y+w.y,v.z+w.z};
}
