#include "Fibonacci.hpp"

int fibonacciRecursif(int n){
	if(n == 0) return 0;
	else if (n == 1) return 1;
	else return fibonacciRecursif(n - 1) + fibonacciRecursif(n - 2);
}

int fibonacciIteratif (int n){
	int f1=0,f2=1,tmp=0;
	for(int i=1;i<n;i++){
		tmp=f2;
		f2=f1+f2;
		f1=tmp;
	}
	if(n==0)return f1;
	return f2;		
}
