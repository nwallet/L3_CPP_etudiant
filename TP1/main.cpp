#include <iostream>
#include "Fibonacci.hpp"
#include "vecteur3.hpp"

int main (){
	std::cout<<"hello !"<<std::endl;
	std::cout<<"Fibonacci Recursif de 7 : "<<fibonacciRecursif(7)<<std::endl;
	std::cout<<"Fibonacci Iteratif de 7 : "<<fibonacciIteratif(7)<<std::endl;

    vecteur3 v {2.0f,3.0f,6.0f};
    vecteur3 w {6.0f,3.0f,2.0f};
    v.afficher();
    afficher(v);
    std::cout<<"norme = "<<v.norme()<<std::endl;
    std::cout<<"addition =";
    afficher(addition(v,w));
    std::cout<<"produit scalaire = "<<produitScalaire(v,w)<<std::endl;
	return 0;
}
