#ifndef VECTEUR3_HPP_
#define VECTEUR3_HPP_

struct vecteur3{
    float x,y,z;

    void afficher () const;
    float norme()const;
};

void afficher(const vecteur3 &v);
float produitScalaire(const vecteur3 &v, const vecteur3 & w);
vecteur3 addition(const vecteur3 &v, const vecteur3 & w);

#endif
