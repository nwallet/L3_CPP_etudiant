#include "Fibonacci.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupFibonacci) { };

TEST(GroupFibonacci, test_fibonacci_1) {  // premier test
    int result = fibonacciRecursif(1);
    CHECK_EQUAL(1, result);
}

TEST(GroupFibonacci, test_fibonacci_2) {  // deuxième test
    int result = fibonacciRecursif(2);
    CHECK_EQUAL(1, result);
}

TEST(GroupFibonacci, test_fibonacci_3) {
    int result = fibonacciRecursif(3);
    CHECK_EQUAL(2, result);
}

TEST(GroupFibonacci, test_fibonacci_4) {
    int result = fibonacciRecursif(4);
    CHECK_EQUAL(3, result);
}

TEST(GroupFibonacci, test_fibonacci_5) {
    int result = fibonacciRecursif(5);
    CHECK_EQUAL(5, result);
}

TEST_GROUP(GroupFibonacciIteratif) { };

TEST(GroupFibonacciIteratif, test_fibonacci_1) {  // premier test
    int result = fibonacciIteratif(1);
    CHECK_EQUAL(1, result);
}

TEST(GroupFibonacciIteratif, test_fibonacci_2) {  // deuxième test
    int result = fibonacciIteratif(2);
    CHECK_EQUAL(1, result);
}

TEST(GroupFibonacciIteratif, test_fibonacci_3) {
    int result = fibonacciIteratif(3);
    CHECK_EQUAL(2, result);
}

TEST(GroupFibonacciIteratif, test_fibonacci_4) {
    int result = fibonacciIteratif(4);
    CHECK_EQUAL(3, result);
}

TEST(GroupFibonacciIteratif, test_fibonacci_5) {
    int result = fibonacciIteratif(5);
    CHECK_EQUAL(5, result);
}
